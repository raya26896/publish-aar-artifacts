# Publish Android AAR into GitLab Maven Repository

The example demonstrates how to publish an Android AAR to a [GitLab Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/). With the combination of GitLab Pipeline, Maven Repository, and Gradle; AAR artifacts can be stored at a GitLab [group](https://docs.gitlab.com/ee/user/packages/maven_repository/#group-level-maven-endpoint) or [project](https://docs.gitlab.com/ee/user/packages/maven_repository/#project-level-maven-endpoint) level for reuse across teams.

## Requirements
* Public GitLab Project or Premium License

## Usage

First, an android library module includes a gradle file to describe library specifications and dependencies. This gradle file will require [maven-publish](https://docs.gradle.org/current/userguide/publishing_maven.html) applied to the file header. 

```script
apply plugin: 'maven-publish'
```


Next, a [publishing block](https://docs.gradle.org/current/userguide/publishing_maven.html#publishing_maven:publications) will need to be included in the same library gradle file. 

Variables:
* `artifact`: Location of the generated aar file
* `PROJECT_ID`: GitLab project ID you want to publish to
* `CI_JOB_TOKEN`: Pre-defined variable included in GitLab pipeline

```
publishing {
    publications {
        aar(MavenPublication) {
            groupId 'com.example'
            artifactId 'library'
            version '0.1'
            artifact("$buildDir/outputs/aar/library-release.aar")
        }
    }
    repositories {
        maven {
            name "gitlab-maven"
            url "https://gitlab.com/api/v4/projects/{PROJECT_ID}/packages/maven"

            credentials(HttpHeaderCredentials) {
                name = "Job-Token"
                value = System.getenv('CI_JOB_TOKEN')
            }
            authentication {
                header(HttpHeaderAuthentication)
            }
        }
    }
}
```

Finally, run the following gradle task in your `.gitlab-ci.yml` file.  
```
Build-And-Publish:
  stage: build
  script:
    - ./gradlew library:build library:publish

```

An AAR artifact will be uploaded to the group or project maven repository as shown below.

![alt text](maven-screenshot.png "AAR in GitLab Maven Repository")
## Contributing
Merge requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


